var http = require('http');
var fs = require('fs');
var readline = require('readline');

// Web-Server:
//---------------------------------------------------------------------------------------------------------------------------------------------
http.createServer(function (req, res)
{
  res.write('Hello World!'); //write a response to the client
  res.end(); //end the response
}).listen(8080); //the server object listens on port 8080

//---------------------------------------------------------------------------------------------------------------------------------------------

//Read-line:
//---------------------------------------------------------------------------------------------------------------------------------------------
var myInterface = readline.createInterface(process.stdin, process.stdout);

var lineno = 0;
myInterface.on('line', function (line)
{
  lineno++;
  console.log('Line number ' + lineno + ': ' + line);
});



//---------------------------------------------------------------------------------------------------------------------------------------------
