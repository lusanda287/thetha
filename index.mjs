


    import * as sensor from "node:http";
    import * as device from "node:os";
    import * as memory from "node:fs";
    import * as readline from "node:readline/promises";
    import { stdin as input, stdout as output } from "node:process";

    // import * as parser from "node:querystring";






    function getLocalIP ()
    {
        const nets = device.networkInterfaces();
        const results = Object.create(null); // Or just "{}", an empty object

        for (const name of Object.keys(nets))
        {
            for (const net of nets[name])
            {
                // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
                // "IPv4" is in Node <= 17, from 18 it"s a number 4 or 6
                const familyV4Value = typeof net.family === "string" ? "IPv4" : 4
                if (net.family === familyV4Value && !net.internal)
                {
                    if (!results[name])
                    { results[name] = [] };
                    results[name].push(net.address);
                }
            }
        };

        return Object.values( results )[0][0];
    };





    class Client
    {
        constructor ( intake, output )
        {
            Object.assign( this,
            {
                addr: (intake.connection.remoteAddress + "").split(":").pop(),
                intake, output
            });
        }
    }





    class Server
    {
        constructor ()
        {
            this.addr = getLocalIP();
            this.host = sensor.createServer( function relay (){ this.handle.apply(this,[...arguments]) }.bind(this) );
            this.host.listen( this.config.port );

            console.log( `sever running on ${this.addr}:${this.config.port}` );
        }



        config =
        {
            port: 1618,
        }



        handle ( intake, output )
        {
            var client = new Client( intake, output );
            var method = ( intake.method );

            this.hash = (this.addr + client.addr ).split(".").join("");
            this.memory = ( "./memory/" + this.hash + ".txt" );
            client.memory = this.memory;

            if ( !memory.existsSync( this.memory ) )
            {
                memory.writeFileSync( this.memory, "\nhi there!\n" );
            };

            // console.log( this.hash, method, intake.url );
            this[ method ]( client );
        }



        GET ( client )
        {
            console.log( "loading conversation: " + this.hash );

            let buffer = memory.readFileSync( this.memory, {encoding:"utf8"} );

            client.output.writeHead( 200, {"Content-Type":"text/plain"} );
            client.output.end( buffer );
        }



        POST ( client )
        {
            let buffer = [],  joiner = "----------------------------";


            client.intake.on( "data", ( data )=>
            {
                buffer.push(data);

                if ( buffer.join("").length > 1000000 )
                { intake.connection.destroy() };
            });

            client.intake.on( "end", ()=>
            {
                buffer = buffer.join("").split( `Content-Disposition: form-data; name="msg"` ).pop().trim().split(joiner)[0].trim();

                memory.writeFileSync( this.memory, (buffer+"\n"), {flag:"a"} );
                console.log( "\n <- " + buffer );
                cli.prompt();

                client.output.writeHead( 200, {"Content-Type":"text/plain"} );
                client.output.end( "." );
            });
        }
    }



    const server = new Server(); // NB starts server



    const cli = readline.createInterface({ input, output });
    cli.setPrompt(" -> ");

    cli.on( "line", ( buffer )=>
    {
        memory.writeFileSync( server.memory, (buffer+"\n"), {flag:"a"} );
        cli.prompt();
    });

    cli.prompt();
